
public class University extends EducationalInstitute{
	
	private final int fixedTax = 100;
	
	public University(String name, String address) {
		super(name, address);
	}

	@Override
	public int getTax() {
		return fixedTax;
	}
	
}
