import java.util.ArrayList;
import java.util.List;

public class School extends EducationalInstitute{
	
	private final int fixedTax = 50;
	
	public School(String name, String address) {
		super(name, address);
	}

	@Override
	public int getTax() {
		// TODO Auto-generated method stub
		return fixedTax;
	}

}
