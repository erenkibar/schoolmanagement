import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public abstract class EducationalInstitute {
	private String id;
	private String name; 
	private String address;
	private List<Student> studentList = new ArrayList<Student>();
	
	public abstract int getTax();
	
	public EducationalInstitute(String name, String address) {
		this.id = UUID.randomUUID().toString();
		this.name = name;
		this.address = address;
	}
	
	//getters
	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public List<Student> getStudentList() {
		return studentList;
	}

	//Adds a student to the institute.
	public void addStudent(Student student) {
		student.setPlace(this);
		student.setEntityId(this.getId());
		studentList.add(student);
		System.out.printf("Hello %s Welcome to %s\n", student.getName(),this.getName());
	}
	
	//Calculates the average of all students in this institute.
	public double calculateAveragePerEntity(){
		double average = 0;
		for (Student st : studentList)
			average += st.getAvgGrade();
		average = average / studentList.size();
		average = Math.round(average * 100.0) / 100.0;
		return average;
	}
	
	//Calcualates income from all students.
	public int calculateIncome() {
		int totalIncome = 0;
		for(Student st : studentList) {
			totalIncome += st.calculatePayment();
		}
		return totalIncome;
	}
	
	@Override
	public String toString() {
		return "Entity [id=" + id + ", name=" + name + ", address=" + address + ", studentList=" + studentList + "]";
	}
	
	
	
}
