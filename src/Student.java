
public class Student {
	private String entityId;
	private String name;
	private int age;
	private String gender; 
	private double avgGrade;
	private EducationalInstitute place;
	
	public Student(String gender, String name, int age,  double avgGrade) {
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.avgGrade = avgGrade;
	}
	
	//getters
	public String getEntityId() {
		return entityId;
	}

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

	public String getGender() {
		return gender;
	}

	public double getAvgGrade() {
		return avgGrade;
	}

	public EducationalInstitute getPlace() {
		return place;
	}
	
	//setters
	public void setEntityId(String id) {
		this.entityId = id;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setAvgGrade(double avgGrade) {
		this.avgGrade = avgGrade;
	}
	
	public void setPlace(EducationalInstitute place) {
		this.place = place;
	}

	//Calculates the payment of a student based on facility's average grade and fixed tax.
	public double calculatePayment() {
		double payment =  this.age / place.calculateAveragePerEntity() * 100 + place.getTax();
		return payment;
	}
	
	@Override
	public String toString() {
		return "Student [entityId=" + entityId + ", name=" + name + ", age=" + age + ", gender=" + gender
				+ ", avgGrade=" + avgGrade + "]";
	}
	
}
