import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Test {

	public static void main(String[] args) {
		
		List<Student> studentList = new ArrayList<Student>();
		List<EducationalInstitute> eduList = new ArrayList<EducationalInstitute>();
		StudentBuilder stbuilder = new StudentBuilder();
				
		eduList.add(new School("Harbor View Elementary","3863 Cambridge Drive"));
		eduList.add(new School("Willow Creek Elementary", "3396 Sumner Street"));
		eduList.add(new School ("Oak Hills Middle School","3347 Pinchelone Street"));
		eduList.add(new University("Vanderbilt University", "1954 Rockford Mountain Lane"));
		eduList.add(new University("University of Southern California","766 Emily Renzelli Boulevard"));
		
		//Add 10 student to schools and generate a studentList that contains all students.
		for (EducationalInstitute edu : eduList) {
			Student st;
			for(int i = 0; i < 10; i++) {
				st = stbuilder.generateStudent();
				edu.addStudent(st);
				studentList.add(st);	
			}
		}
		
		//Average grade per university/school
		System.out.println("\nAverage grade of each school\n");
		for (EducationalInstitute edu : eduList) {
			System.out.printf("%s %s\n", edu.getName(), edu.calculateAveragePerEntity());	
		}
		
		//Top performer among all university/schools
		topStudent(studentList);
		
		//Top male student
		topMale(studentList);
		
		//Top female student
		topFemale(studentList);
		
		//Calculate income for all schools
		for(EducationalInstitute edu : eduList) {
			System.out.printf("\nIncome for %s is %s BGN\n", edu.getName(), edu.calculateIncome());
		}
		
		//Top contributing institute
		eduList.sort(Comparator.comparingDouble(EducationalInstitute::calculateIncome).reversed());
		System.out.printf("Top contributor is %s with income of %s BGN\n", eduList.get(0).getName(), 
																	   	   eduList.get(0).calculateIncome());
	}

	private static void topStudent(List<Student> studentList) {
		studentList.sort(Comparator.comparingDouble(Student::getAvgGrade).reversed());
		System.out.printf("\nTop performer of all students is %s with average %s\n", 
				studentList.get(0).getName(),
				studentList.get(0).getAvgGrade());
	}

	private static void topMale(List<Student> studentList) {
		List<Student> males = studentList.stream().filter(student -> student.getGender()
				.equals("male"))
				.collect(Collectors.toList());
		males.sort(Comparator.comparingDouble(Student::getAvgGrade).reversed());
		System.out.printf("Top performing male student is %s with average %s\n", 
							males.get(0).getName(), males.get(0).getAvgGrade());
	}

	private static void topFemale(List<Student> studentList) {
		List<Student> females = studentList.stream().filter(student -> student.getGender()
				.equals("female"))
				.collect(Collectors.toList());
		females.sort(Comparator.comparingDouble(Student::getAvgGrade).reversed());
		System.out.printf("Top performing female student is %s with average %s\n", 
				females.get(0).getName(), females.get(0).getAvgGrade());
	}
	
}
