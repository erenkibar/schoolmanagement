import java.lang.reflect.Array;
import java.util.List;
import java.util.Random;

public class StudentBuilder {
	
	private String[] maleNames = {"John", "Matt", "Jack", "Zackery", "Homer", "Harry", "Austin","Brian","Bryant","Don"};
	private String[] femaleNames = {"Ashtyn","Marin","Madyson","Macy","Alexus","Rubi","Melody","Kennedy","Denise","Brenda"};
	private String[] surnames = {"Lee","Cole","Lafriniere","Ducharme","Force","Dandridge", "Infinite","Vanchure","Hart","Owens"};
	private String[] genders = {"male","female"};
	private boolean isMale;
	private static int counter = 0;
	
	
	//Picks a random gender.
	public String genderPicker() {
		String gender = null;
 		if (counter % 2 == 0 ) {
 			gender = genders[0];
			this.isMale = true;
		} else
			gender = genders[1];
			this.isMale = false;
 		counter++;
		return gender;
	}
	
	//Creates random full name based on the gender. 
	public String randomName() {
		String first = isMale ? (String) Array.get(maleNames, new Random().nextInt(maleNames.length)) 
							: (String) Array.get(femaleNames, new Random().nextInt(femaleNames.length));
		int last = new Random().nextInt(surnames.length);
		String fullName = first  + " " + surnames[last];
		return fullName;
	}
	//Creates a random age.
	public int randomAge(){
		Random  r = new Random();
		return r.nextInt((30 - 14) + 1) + 14;
	}
	
	//Creates a random grade.
	public double randomAvgGrade() {
		
	    double grade = 2D + new Random().nextDouble() * (6D - 2D);
		grade = Math.round(grade * 100.0) / 100.0;
		return grade;
	}
	
	//Creates and returns a Student object.
	public Student generateStudent(){
		Student temp = new Student(genderPicker(),randomName(),randomAge(),randomAvgGrade());
		return temp;
	}
	
}
